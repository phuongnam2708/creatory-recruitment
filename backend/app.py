from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from sqlalchemy import Column, func, desc
from sqlalchemy import (
    Integer, String, DateTime, Text,
    ForeignKey, text
)
from flask_cors import CORS, cross_origin
import os

backend_path = os.path.dirname(os.path.abspath(__file__))
db_file_path = os.path.join(backend_path, "db.sqlite3")

app = Flask(__name__)
cors = CORS(app)
app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{db_file_path}"
app.config['CORS_HEADERS'] = 'Content-Type'
db = SQLAlchemy(app)


class VideoMeasurement(db.Model):
    __tablename__ = 'video_measurement'

    id = Column(Integer, primary_key=True, autoincrement=True)
    video_id = Column(Integer, ForeignKey('video.id', ondelete="CASCADE"))
    video = relationship("Video", back_populates="measurements")
    measurement_date = Column(DateTime())
    sub_count = Column(Integer, server_default=text("0"))
    comments = Column(Integer, server_default=text("0"))
    subscribersgained = Column(Integer, server_default=text("0"))
    subscriberslost = Column(Integer, server_default=text("0"))
    unsub_views = Column(Integer, server_default=text("0"))
    unsub_likes = Column(Integer, server_default=text("0"))
    unsub_dislikes = Column(Integer, server_default=text("0"))
    unsub_shares = Column(Integer, server_default=text("0"))

    def as_json(self):
        return {
            'id': self.id,
            'video_id': self.video_id,
            'measurement_date': self.measurement_date.isoformat(),
            'comments': self.comments,
            'subscribersgained': self.subscribersgained,
            'subscriberslost': self.subscriberslost,
            'unsub_views': self.unsub_views,
            'unsub_likes': self.unsub_likes,
            'unsub_dislikes': self.unsub_dislikes,
            'unsub_shares': self.unsub_shares,
        }


class Video(db.Model):
    __tablename__ = 'video'

    id = Column(Integer, primary_key=True, autoincrement=True)
    youtube_id = Column(String(128))
    channel_id = Column(Integer, ForeignKey('channel.id'))
    channel = relationship("Channel", back_populates="videos")
    create_date = Column(DateTime())
    title = Column(String(128))
    description = Column(Text())
    duration = Column(Integer)
    measurements = relationship(
        "VideoMeasurement", cascade="all,delete",
        back_populates="video", passive_deletes=True)


class Channel(db.Model):
    __tablename__ = 'channel'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(128))
    videos = relationship("Video")


@app.route('/results', methods=['GET'])
@cross_origin()
def results():
    results = db.session.query(VideoMeasurement).all()

    return(jsonify([result.as_json() for result in results]))


@app.route('/lastest_measurement_by_videoid/<int:video_id>', methods=['GET'])
@cross_origin()
def lastest_measurement_by_videoid(video_id):
    measurements_video = db.session.query(VideoMeasurement)\
        .filter_by(video_id=video_id).order_by(desc(VideoMeasurement.measurement_date)).first()
    return measurements_video.as_json()


@app.route('/measure_groupby_video', methods=['GET'])
@cross_origin()
def measurements_groupby_video():
    results_groupby_video = db.session.query(
        Video.id, Video.title, func.sum(VideoMeasurement.unsub_views)) \
        .join(Video, Video.id == VideoMeasurement.video_id) \
        .group_by(Video.id, Video.title).all()
    result_array = []
    for result in results_groupby_video:
        result_dict = {
            "video_id": "",
            "video_title": "",
            "unsub_views": "",
        }
        for index, item in enumerate(result):
            if index == 0:
                result_dict['video_id'] = item
            if index == 1:
                result_dict['video_title'] = item
            if index == 2:
                result_dict['unsub_views'] = item
        result_array.append(result_dict)
    return jsonify(result_array)


@app.route('/measure_groupby_channel', methods=['GET'])
@cross_origin()
def measurements_groupby_channel():
    results_groupby_channel = db.session.query(
        Channel.id, Channel.name, func.sum(VideoMeasurement.unsub_views)) \
        .join(Video, Video.id == VideoMeasurement.video_id) \
        .join(Channel, Channel.id == Video.channel_id) \
        .group_by(Channel.id, Channel.name).all()
    result_array = []
    for result in results_groupby_channel:
        result_dict = {
            'channel_id': "",
            "channel_name": "",
            "unsub_views": "",
        }
        for index, item in enumerate(result):
            if index == 0:
                result_dict['channel_id'] = item
            if index == 1:
                result_dict['channel_name'] = item
            if index == 2:
                result_dict['unsub_views'] = item
        result_array.append(result_dict)
    return jsonify(result_array)


if __name__ == '__main__':
    app.run()
