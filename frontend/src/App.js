import React, { useState, useEffect } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, Row, Col, 
  CardImg, CardText, CardBody,
  CardTitle, CardSubtitle , Media, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import classnames from 'classnames';
import axios from 'axios';
import './App.css';
import mainLogo from'./assets/hcmusImage.jpg';

function App() {
  const [apiData, setApiData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [activeTab, setActiveTab] = useState('1');
  const [channelData, setChannelData] = useState([]);
  const [videoData, setVideoData] = useState([]);

  const [modal, setModal] = useState(false);

  const toggleModal = () => setModal(!modal);

  const [measurementData, setMeasurementData] = useState({});

  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }

  useEffect(() => {
    const fetchVideoData = async () => {
      try{
        const response = await axios.get(
          'http://127.0.0.1:5000/measure_groupby_video'
        );
        setVideoData(response.data);
      }
      catch(error){
        console.log(error)
      }
    }

    const fetchChannelData = async () => {
      try{
        const response = await axios.get(
          'http://127.0.0.1:5000/measure_groupby_channel'
        );
        setChannelData(response.data);
      }
      catch(error){
        console.log(error)
      }
    }

    const fetchData = async () => {
        setIsLoading(true);
        setIsError(false);
        try {
          const result = await axios.get(
            'http://127.0.0.1:5000/results'
          );
          setIsLoading(false);
          setApiData(result.data);
        }
        catch (error) {
          console.log(error)
          setIsLoading(false);
          setIsError(true);
        }
    };

    fetchVideoData();
    fetchData();
    fetchChannelData();
  }, []);

  async function clickCallMeasurement(video_id) {
    try{
      const response = await axios.get(
        `http://127.0.0.1:5000/lastest_measurement_by_videoid/${video_id}`
      );
      setMeasurementData(response.data);
      toggleModal()
    }
    catch(error){
      console.log(error)
    }
  }

  return (
    <div className="App">
      <h1>Measurement Page</h1>
      <Nav tabs style={{padding:'100px 100px 0px 100px'}}>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            Channels
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
            Videos
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row style={{justifyContent:'space-around'}}>
            {
              channelData.map(item => (
                <Card>
                  <CardImg top width="50px" height="100px" src={mainLogo} alt="Card image cap" />
                  <CardBody>
                    <CardTitle>Name: {item.channel_name}</CardTitle>
                    <CardSubtitle>Views: {item.unsub_views}</CardSubtitle>
                  </CardBody>
                </Card>
              ))
            }
          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
          <Col sm="6">
            {
              videoData.map(item => (
                <Media>
                  <Media left >
                    <Media object height="100px" width="100px" src={mainLogo} alt="Generic placeholder image" />
                  </Media>
                  <Media body>
                    <Media heading>
                    {item.video_title}
                    </Media>
                    Views: {item.unsub_views}
                    
                  </Media>
                  <Button id={item.video_id} onClick={()=>clickCallMeasurement(item.video_id)} color="primary">See measurement detail</Button>{' '}

                  <Modal isOpen={modal} toggle={toggleModal}>
                    <ModalHeader toggle={toggleModal}>Measurement Detail {item.title}</ModalHeader>
                    <ModalBody>

                      <div>
                        Date Measurement: {measurementData.measurement_date}
                      </div>
                      <div>
                        Like: {measurementData.unsub_likes}
                      </div>
                      <div>
                        Dislike: {measurementData.unsub_dislikes}
                      </div>
                      <div>
                        Views: {measurementData.unsub_views}
                      </div>
                      <div>
                        Shares: {measurementData.unsub_shares}
                      </div>
                      <div>
                        Comments: {measurementData.comments}
                      </div>
                      <div>
                        Subscribers Gained:{measurementData.subscribersgained}
                      </div>
                      <div>
                      Subscribers Lost: {measurementData.subscriberslost}
                      </div>
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary" onClick={toggleModal}>Cancel</Button>
                    </ModalFooter>
                  </Modal>

                </Media>
              ))
            }
            </Col>
          </Row>
        </TabPane>
      </TabContent>



    </div>
  );
}

export default App;
